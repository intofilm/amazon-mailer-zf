<?php

namespace IntoFilm\Zend\Mail\Transport;

use Aws\Ses\SesClient;
use Zend_Mail_Transport_Abstract;

class AmazonSes extends Zend_Mail_Transport_Abstract
{
    /**
     * @var SesClient
     */
    protected $client;

    public function __construct(array $options = [])
    {
        $this->client = SesClient::factory($options);
    }

    protected function _sendMail()
    {
        $params = [
            'Action' => 'SendRawEmail',
            'Source' => $this->_mail->getFrom(),
            'RawMessage' => ['Data' => base64_encode(sprintf("%s\n%s\n", $this->header, $this->body))],
            'Destinations' => explode(',', $this->recipients),
        ];

        $this->client->sendRawEmail($params);
    }
}