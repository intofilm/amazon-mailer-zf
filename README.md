# ZF1 AWS SES Mail Transport

Quick and dirty SES mail transport for use in ancient ZF1 projects. Locked to specific versions in `require-dev` to make
life easier.

## Usage

`composer require intofilm/amazon-mailer-zf`

```
use IntoFilm\Zend\Mail\Transport\AmazonSes;

Zend_Mail::setDefaultTransport(new AmazonSes(['region' => 'eu-west-1', 'credentials' => [...]]));
```